package platform

import (
	"code.google.com/p/go-uuid/uuid"
	"encoding/json"
	"log"
	"messaging"
	"messaging/amqp"
	"os"
)

const (
	Request  = "ciena.sdn.requests"
	Response = "ciena.sdn.response"
)

var (
	l = log.New(os.Stderr, "[platform] ", log.LstdFlags)
)

type Message map[string]interface{}

type Provider interface {
	HandleRequest(message Message)
}

type registrations map[string]interface{}

type Client struct {
	comms  messaging.MessagingProvider
	id     uuid.UUID
	queues map[string]registrations
}

func Connect() (*Client, error) {
	if comms, err := amqp.NewMessagingProvider(); err != nil {
		return nil, err
	} else {
		return ConnectWithProvider(comms)
	}
}

func ConnectWithProvider(comms messaging.MessagingProvider) (*Client, error) {
	var client *Client = new(Client)
	client.id = uuid.NewUUID()
	client.queues = make(map[string]registrations)
	client.comms = comms
	return client, nil
}

func (client *Client) Serve() {

}

func (client *Client) ID() uuid.UUID {
	return client.id
}

func (client *Client) Disconnect() error {
	return nil
}

type funcHandlerWrapper struct {
	handler func(Message) error
}

func newFuncHandlerWrapper(handler func(Message) error) *funcHandlerWrapper {
	wrapper := new(funcHandlerWrapper)
	wrapper.handler = handler
	return wrapper
}

func (*funcHandlerWrapper) wrapFuncHandler(data []byte) error {
	return nil
}

func (client *Client) HandleFunc(exchange string, pattern string, handler func(Message) error) error {
	// if val, ok := client.queues[exchange]; !ok || val != pattern {
	// 	l.Printf("Did not find %s[%s]\n", exchange, pattern)

	// 	if err := client.comms.Register(exchange, pattern, newFuncHandlerWrapper(handler).wrapFuncHandler); err == nil {
	// 		client.queues[exchange] = pattern

	// 	}
	// }
	return nil
}

type handlerWrapper struct {
	handler Provider
}

func newHandlerWrapper(handler Provider) *handlerWrapper {
	wrapper := new(handlerWrapper)
	wrapper.handler = handler
	return wrapper
}

func (*handlerWrapper) wrapHandler(data []byte) error {
	return nil
}

func (client *Client) Handle(exchange string, pattern string, handler Provider) error {
	if _, ok := client.queues[exchange]; !ok {
		// Have not registered against this exchange yet, so init
		client.queues[exchange] = make(map[string]interface{})
	}

	if _, ok := client.queues[exchange][pattern]; !ok {
		// No registration of this pattern
		if err := client.comms.Register(exchange, pattern, client.id.String(), newHandlerWrapper(handler).wrapHandler); err == nil {
			l.Printf("Remembering %s[%s]\n", exchange, pattern)
			client.queues[exchange][pattern] = pattern
		} else {
			return err
		}
	} else {
		l.Printf("Already have a registration for %s[%s]", exchange, pattern)
	}
	return nil
}

func (client *Client) Publish(exchange string, topic string, message Message) error {
	if data, err := json.Marshal(message); err != nil {
		return err
	} else {
		client.comms.Publish(exchange, topic, data)
	}
	return nil

}

func (client *Client) PublishRaw(exchange string, topic string, message []byte) error {
	return nil

}
