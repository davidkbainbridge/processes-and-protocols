package messaging

type MessagingProvider interface {
	Register(exchange string, pattern string, clientID string, wrapper func([]byte) error) error
	Unregister(exchange string, pattern string)
	Publish(exchange string, topic string, data []byte) error
}
