package amqp

import (
	"github.com/streadway/amqp"
	"log"
	"os"
)

var (
	l              = log.New(os.Stderr, "[messaging/amqp] ", log.LstdFlags)
	DialURI string = "amqp://guest:guest@localhost:5672/"
)

type MessagingProvider struct {
	connection *amqp.Connection
	channel    *amqp.Channel
}

func NewMessagingProvider() (*MessagingProvider, error) {
	var err error

	l.Println("Create new provider instance")
	provider := new(MessagingProvider)
	if provider.connection, err = amqp.Dial(DialURI); err != nil {
		return nil, err
	}
	if provider.channel, err = provider.connection.Channel(); err != nil {
		return nil, err
	}
	return provider, nil
}

func (p *MessagingProvider) Register(exchange string, pattern string, clientID string, handler func([]byte) error) error {
	l.Println("Register Handler -- ")
	var err error

	// Check to see if we already have a queue attached to the named
	// exchange, if not, then make sure the exchange is declared and
	// the queue is declared

	if err = p.channel.ExchangeDeclare(
		exchange, // name
		"topic",  // kind
		true,     // durable
		true,     // autoDelete
		false,    // internal
		false,    // noWait
		nil,      // args
	); err != nil {
		return err
	}

	l.Printf("EXCHANGE DECLARED: %s\n", exchange)

	if _, err = p.channel.QueueDeclare(
		clientID+":"+exchange+":"+pattern, // name
		false, // durable
		true,  // autoDelete
		true,  // exclusive
		false, // noWait
		nil,   // args
	); err != nil {
		return err
	}

	l.Println("QUEUE DECLARED")
	if err = p.channel.QueueBind(
		clientID+":"+exchange+":"+pattern, // name
		pattern,  // key
		exchange, // exchange
		false,    // noWait
		nil,      // args
	); err != nil {
		l.Printf("ERR: %s", err)
		return err
	}
	l.Println("QUEUE BOUND")

	var deliveries <-chan amqp.Delivery
	if deliveries, err = p.channel.Consume(
		clientID+":"+exchange+":"+pattern, // queue
		clientID+":"+exchange+":"+pattern, // consumer
		true,  // autoAck
		false, // exclusive
		false, // noLocal
		false, // noWait
		nil,   // args
	); err != nil {
		return err
	}
	l.Println("BEFORE")
	go func(deliveries <-chan amqp.Delivery) {
		l.Println("RANGE OVER INPUT")
		for d := range deliveries {
			l.Printf("MSG: %s\n", d)
		}
		l.Println("RANGE DONE")
	}(deliveries)

	return nil

}

func (p *MessagingProvider) Unregister(exchange string, pattern string) {
	l.Println("Unregister handler")

}

func (p *MessagingProvider) Publish(exchange string, topic string, data []byte) error {
	l.Printf("Publish message : %s[%s]\n", exchange, topic)
	if err := p.channel.Publish(exchange, topic, false, false, amqp.Publishing{
		Headers:         amqp.Table{},
		ContentType:     "text/plain",
		ContentEncoding: "",
		DeliveryMode:    amqp.Transient,
		Priority:        0,
		Body:            data,
	}); err != nil {
		return err
	}
	return nil
}
