Process and Protocols
=====================

Overview
--------
The point of this project is to perform some experiments using a messaging capability as a mechanism for component communication in a large system where each component may provide only a small portion of the 
overall behavior.

Proof Points
------------
Below are a series of proof points that this expriment is meant to answer

### Request / Response Sequence
The idea is that there is a queue (or topic) onto which request objects are posted. The request is routed
by use of topics on this queue so that it is routed to a handler that has registered to handle a given
request type.

When considering this proof point it is important to also deal with security as well as multiple instances
of a given handler. In terms of security it is important that a message only get to a handler that is 
authorized and in terms of multiple instances only a single instance should handle each request.

### RESTful Presentation of API
The idea is to expose the underlying message based API via a RESTful interface, where by a "status" 
resource is returned to the client and the implementation updates that status as the request is processed.
This implies that even in the message structure underneath, there is a concept of a status that is
updated as processing proceeds.

### Message Based Data Storage
It should be investigate how data storage can be incorporatetd into this scheme such that calls to
data storage are made via async messages as well.

### Non-Request Messages
It should be seen how non-request events, such as device discovery, are integrated into this system.

Technology
----------

### AMQP
Initially, an AMQP implementation will be used for messaging. AMQP represents server based message system
where every messages is transfered through a server and routed and filtered accordingly. It may be
interesting at some point to understand if a distributed message technology, like ZeroMQ, might be
leveraged.