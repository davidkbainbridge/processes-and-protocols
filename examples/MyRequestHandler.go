package main

import (
	"log"
	"platform"
)

type MyHandler struct {
}

func (*MyHandler) HandleRequest(message platform.Message) {

}

func main() {
	var (
		client *platform.Client
		err    error
	)

	if client, err = platform.Connect(); err != nil {
		log.Printf("CONNECT ERROR: %s", err)
		return
	}
	client.Handle(platform.Request, "path.*", new(MyHandler))
	//client.Handle(platform.Request, "path2.*", new(MyHandler))
	//client.HandleFunc(platform.Request, "path2.*", func(message platform.Message) error {
	//	return nil
	//})
	// client.Publish(platform.Request, "ciena.sdn.request.foo",
	// 	platform.Message{
	// 		"name1": "value1",
	// 		"name2": 1,
	// 		"name3": platform.Message{
	// 			"sub1": "value1",
	// 			"sub2": 4,
	// 			"sub4": 4.2,
	// 		},
	// 	})

	client.Serve()
	select {}
}
