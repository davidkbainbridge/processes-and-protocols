package main

import (
	"platform"
)

func main() {
	if client, err := platform.Connect(); err == nil {
		client.Publish(platform.Request, "path.test1", platform.Message{"hello": "world"})
	}
}
